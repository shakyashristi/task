<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Session;
use File;
use Response;

class Home extends Controller
{
    
    public function index()
    {
        return view('index');
    }
}
