<?php

namespace App\Http\Controllers\clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;

use Excel;

use App\ClientClass;

class Client extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $objClient = new ClientClass;
        $objClient->setPaginate(10);
        $objClient->setPage($request->page);
        $users = $objClient->getPaginate();

        return view('clients.index', ['users' => $users['data'],
                                      'link' => $users['links']
                                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
                'name' =>'required|max:255',
                'gender' => 'required',
                'phone' => 'required',
                'email' => 'required|email',
                'address' =>'required|min:3',
                'education_background' => 'nullable|min:3',
                'captcha' => 'required|captcha'
                
        ));

        $objClient = new ClientClass;
        $total_client = $objClient->countRecords();

        if($objClient->writeUser($total_client, $request))
        {
            Session::flash('success', 'Added');
        }else{
            Session::flash('info', 'Try Again');
        }
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objClient = new ClientClass;
        $objClient->setId($id);
        $user = $objClient->getOneUser(); 

        if (count($user) > 0 ) {
            return view('clients.show', ['user' => $user]);
        } 

        Session::flash('warning', 'No user found');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //export to excel and pdf
    public function export(Request $request)
    {
        $objClient = new ClientClass;
        $users = $objClient->getAllUser();        

        Excel::create('Client', function($excel) use($users) {

            
            $excel->sheet('clients', function($sheet) use($users) {
                $sheet->row(1, array('ID', 'Name', 'Gender', 'Phone', 'Email', 'Address', 'Nationaliy', 'Date of Birth', 'Educational Background', 'Mode of Contact'));
                $count = 1;
                foreach ($users as $key => $value) {
                    $sheet->row(++$count, array($value['id'],
                                                $value['name'],
                                                $value['gender'],
                                                $value['phone'],
                                                $value['email'],
                                                $value['address'],
                                                $value['nationality'],
                                                $value['dob'],
                                                $value['education_background'],
                                                $value['mode_of_contact']
                                                 ));
                }
                
                
            });
            

        })->export('xlsx');
    }
}
