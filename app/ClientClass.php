<?php

namespace App;

use League\Csv\Writer;
use League\Csv\Reader;

use App\Paginate;

class ClientClass{

	protected $path = 'clients/client.csv';

    private $id, $paginate;

    private $page = 1;

    //setters
    public function setId($var){
        $this->id = $var;
    }

    public function setPaginate($var){
        $this->paginate = $var;
    }

    public function setPage($page){
        if ($page) {
            $this->page = $page;
        } 
    }

    //getters

    //get all users
	public function getAllUser(){
		$objReader = Reader::createFromPath(public_path($this->path), 'r');
        $keys = ['id', 'name', 'gender', 'phone', 'email', 'address', 'nationality', 'dob', 'education_background', 'mode_of_contact'];
        return $objReader->fetchAssoc($keys);
	}

    //get one user
    public function getOneUser(){
        $user = [];        
        $all_users = $this->getAllUser();

        foreach ($all_users as $key => $value) {
            if ($value['id'] == $this->id) {
                $user = $value;
            }
        }

        return $user;
    }

    public function getPaginate(){

        $users = $this->getAllUser(); 

        $objPaginate = new Paginate($users, $this->countRecords());        
        $user_paginate = $objPaginate->getPaginate($this->paginate, $this->page);

        return $user_paginate;
    }


    //other functions

	public function countRecords(){
		$objReader = Reader::createFromPath(public_path($this->path), 'r');
        return count($objReader->fetchAll());
	}


    //to write in the file
	public function writeUser($total_record, $request){
		$objWriter = Writer::createFromPath(public_path('clients/client.csv'), 'a');

        if($objWriter->insertOne([$total_record+1, 
                            $request->name,
                            $request->gender,
                            $request->phone,
                            $request->email,
                            $request->address,
                            $request->nationality,
                            $request->dob,
                            $request->education_background, 
                            $request->mode_of_contact]) )
        {
        	return true;
        }

        return false;
	}

    

	
}