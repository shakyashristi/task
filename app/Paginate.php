<?php

namespace App;

class Paginate{
	private $limit = 'all';
    private $page;
    private $query;
    private $total;

    public function __construct( $query, $total ) {
     
	    
	    $this->query = $query;
	 
	    $this->total = $total;
	     
	}

	public function getPaginate($limit, $page) {
     	
	    $this->limit = $limit;
	    $this->page = $page;
	 
	    if ($this->limit == 'all') {
	        $query = $this->query;
	    } else {
	        $query = [];
	        $start = ($this->page - 1) * $this->limit;
	        $end = $this->limit * $this->page; 
	        
	        foreach ($this->query as $key => $value) {
	        	if ($key >= $start && $key < $end) {
	        		array_push($query, $value);
	        	}
	        }
	    }
	 
	    $result = [];
	    $result['page'] = $this->page;
	    $result['limit'] = $this->limit;
	    $result['total'] = $this->total;
	    $result['data'] = $query;
	    $result['links'] = $this->createLinks(7, 'pagination pagination-sm');
	 
	    return $result;
	}

	public function createLinks( $links, $list_class ) {
	    if ( $this->limit == 'all' ) {
	        return '';
	    }
	 
	    $last       = ceil( $this->total / $this->limit );
	 
	    $start      = (($this->page - $links) > 0) ? $this->page - $links : 1;
	    $end        = (($this->page + $links) < $last) ? $this->page + $links : $last;
	 
	    $html       = '<ul class="'.$list_class.'">';
	 
	    $class      = ($this->page == 1) ? "disabled" : "";
	    $html       .= '<li class="'.$class.'"><a href="?page='.($this->page - 1).'">&laquo;</a></li>';
	 
	    if ( $start > 1 ) {
	        $html   .= '<li><a href="?page=1">1</a></li>';
	        $html   .= '<li class="disabled"><span>...</span></li>';
	    }
	 
	    for ( $i = $start ; $i <= $end; $i++ ) {
	        $class  = ( $this->page == $i ) ? "active" : "";
	        $html   .= '<li class="'.$class.'"><a href="?page='.$i.'">'.$i.'</a></li>';
	    }
	 
	    if ( $end < $last ) {
	        $html   .= '<li class="disabled"><span>...</span></li>';
	        $html   .= '<li><a href="?page='.$last.'">'.$last.'</a></li>';
	    }
	 
	    $class      = ( $this->page == $last ) ? "disabled" : "";
	    $html       .= '<li class="'. $class.'"><a href="?page='.($this->page + 1).'">&raquo;</a></li>';
	 
	    $html       .= '</ul>';
	 
	    return $html;
	}
}