@extends('template')

@section('content')
	<div class="col-md-12">
		<a href="{{ route('client.index') }}"><button class="btn btn-primary">View Clients</button></a>
		<a href="{{ route('client.create') }}"><button class="btn btn-success">Add Client</button></a>
	</div>
@endsection