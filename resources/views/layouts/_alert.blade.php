@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <strong>Well done!</strong> {{Session::get('success')}}
</div>
@endif

@if(Session::has('info'))
<div class="alert alert-info" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <strong>Info!</strong> {{Session::get('info')}}
</div>
@endif 

@if(Session::has('warning'))
<div class="alert alert-warning" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <strong>Warning!</strong>{{Session::get('warning')}}
</div>
@endif

@if (Session::has('fail'))
<div class="alert alert-danger" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <strong>Fail!</strong> {{Session::get('fail')}}
</div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <strong>Error!</strong> 
  <ul>
	@foreach($errors->all() as $errors)
   		<li>{{$errors}}</li>
    @endforeach
  </ul>
</div>
@endif