@extends('template')

@section('content')
	<div class="col-md-12">	
		<div class="head">
			<a href="{{ route('client.index') }}" class="btn btn-warning">Back</a>
		</div>	
		<div class="card">
		  <ul class="list-group list-group-flush">
		    <li class="list-group-item">Name: {{ $user['name'] }}</li>
		    <li class="list-group-item">Gender:{{ $user['gender'] }}</li>
		    <li class="list-group-item">Phone:{{ $user['phone'] }}</li>
		    <li class="list-group-item">Email:{{ $user['email'] }}</li>
		    <li class="list-group-item">Address:{{ $user['address'] }}</li>
		    <li class="list-group-item">Nationality:{{ $user['nationality'] }}</li>
		    <li class="list-group-item">DOB:{{ $user['dob'] }}</li>
		    <li class="list-group-item">Educational Background:{{ $user['education_background'] }}</li>
		    <li class="list-group-item">Mode of Contact:{{ $user['mode_of_contact'] }}</li>
		  </ul>
		</div>
	</div>
@endsection