@extends('template')

@section('content')
	<div class="col-md-12">
		<div class="btn-group" role="group" aria-label="Basic example">
		  <a href="{{ route('client.export') }}"><button type="button" class="btn btn-secondary">Excel</button></a>
		</div>
		<a href="{{ route('client.create') }}"><button type="button" class="btn btn-secondary pull-right">Add Client</button></a>
		<table class="table">
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Name</th>
		      <th>Gender</th>
		      <th>Date of Birth</th>
		      <th>Address</th>
		      <th>Phone</th>
		      <th>Email</th>
		      <th>Mode of Contact</th>
		      <th>Nationality</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($users as $key => $value)
		    <tr>
		      <th scope="row">{{ $value['id'] }}</th>
		      <td>{{ $value['name'] }}</td>
		      <td>{{ $value['gender'] }}</td>
		      <td>{{ $value['dob'] }}</td>
		      <td>{{ $value['address'] }}</td>
		      <td>{{ $value['phone'] }}</td>
		      <td>{{ $value['email'] }}</td>
		      <td>{{ $value['mode_of_contact'] }}</td>
		      <td>{{ $value['nationality'] }}</td>
		      <td><a class="btn btn-xs btn-primary" href="{{ route('client.show', $value['id']) }}">View Details</a></td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
		{!! $link !!}
	</div>
@endsection