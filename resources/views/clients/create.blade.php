@extends('template')

@section('content')
	<div class="col-md-12">
		<h2>Fill Up </h2>
	  <div id="app">
        <div class="panel panel-default">
          <div class="panel-heading">Add Personal Information</div>
          <div class="panel-body">
              
            <div class="col-md-9">
              <form class="form-horizontal" method="POST" action="{{ route('client.store') }}" data-parsley-validate="">
              	{{ csrf_field() }}
              <fieldset>
              <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Name*</label>  
                <div class="col-md-4">
                <input name="name" type="text" placeholder="Full Name" class="form-control input-md" v-model="name" value="{{ Request::old('name') }}" required="">
                  
                </div>
              </div>

              <!-- Multiple Radios (inline) -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="gender">Gender*</label>
                <div class="col-md-4"> 
                  <label class="radio-inline" for="gender-0">
                    <input type="radio" name="gender" id="gender-0" value="Male" checked="checked">
                    Male
                  </label> 
                  <label class="radio-inline" for="gender-1">
                    <input type="radio" name="gender" id="gender-1" value="Female">
                    Female
                  </label> 
                  <label class="radio-inline" for="gender-2">
                    <input type="radio" name="gender" id="gender-2" value="Other">
                    Other
                  </label>
                </div>
              </div>

              <!-- Prepended text-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="phone">Phone*</label>
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">+977</span>
                    <input id="phone" name="phone" class="form-control" placeholder="98xxxxxxxx" type="number" v-model="phone" value="{{ Request::old('phone') }}" required>
                  </div>
                  
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email*</label>  
                <div class="col-md-4">
                <input id="email" name="email" type="email" placeholder="example@gmail.com" class="form-control input-md" value="{{ Request::old('email') }}" required="">
                  
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="address">Address*</label>  
                <div class="col-md-4">
                <input id="address" name="address" type="text" placeholder="Street, City" class="form-control input-md" v-model="address" value="{{ Request::old('address') }}" required="">
                <span class="help-block">Eg: Baneshwor</span>  
                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="nationality">Nationality</label>
                <div class="col-md-4">
                  <select id="nationality" name="nationality" class="form-control">
                    <option value="Nepali">Nepali</option>
                    <option value="Hindi">Hindi</option>
                  </select>
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="dob">Date Of Birth</label>  
                <div class="col-md-4">
                <input id="dob" name="dob" type="text" placeholder="YYYY-MM-DD" class="form-control input-md" autocomplete="false" value="{{ Request::old('dob') }}" v-model="dob">
                <span class="help-block">date of birth in AD</span>  
                </div>
              </div>

              <!-- Textarea -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="education_background">Education Background</label>
                <div class="col-md-4">                     
                  <textarea class="form-control" id="education_background" name="education_background" placeholder="Academy Qualification.." rows="4"  value="{{ Request::old('education_background') }}"  data-parsley-trigger="keyup" data-parsley-minlength="10" data-parsley-maxlength="100" data-parsley-minlength-message="You need to enter at least a 10 character qualification or leave it empty.." data-parsley-validation-threshold="10"></textarea>
                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="mode_of_contact">Preferred Mode Of Contact</label>
                <div class="col-md-4">
                  <select id="mode_of_contact" name="mode_of_contact" class="form-control">
                    <option value="Email">Email</option>
                    <option value="Phone">Phone</option>
                    <option value="None">None</option>
                  </select>
                </div>
              </div>
              

              <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                <label for="captcha" class="col-md-4 control-label">Input captcha*</label>
                <div class="col-md-4">
                  <input id="captcha" type="text" class="form-control" name="captcha" autocomplete="off">
                    @if ($errors->has('captcha'))
                        <span class="help-block">
                            <strong>{{ $errors->first('captcha') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
              <div class="col-md-6 col-md-offset-4">
                  {!! captcha_img('flat') !!}
              </div>


              <!-- Button -->
              <div class="form-group">
                <label class="col-md-4 control-label" for=""></label>
                <div class="col-md-4">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>

              </fieldset>
              </form>
            </div>

            <div class="col-md-3">
              <input type="checkbox" v-on:click="toogleID" value="1"> Show ID Card
              <div class="panel panel-default" v-if="show_idcard">
                <div class="panel-heading">ID Card</div>
                  <div class="panel-body">
                    <ul class="list-group">
                      <li class="list-group-item">Name: @{{name}}</li>
                      <li class="list-group-item">Address: @{{ address }}</li>
                      <li class="list-group-item">Phone: @{{ phone }}</li>
                      <li class="list-group-item">DOB: @{{ dob }}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
      </div>
	</div>
@endsection

@section('javascripts')
	<script type="text/javascript">
      
      var app = new Vue({
        el: '#app',
        data: {
          name: '',
          address: '',
          phone: '',
          dob: '',
          show_idcard: false
        },
        methods: {
          toogleID: function () {
            this.show_idcard = !this.show_idcard
          }
        }
      })
    </script>
@endsection