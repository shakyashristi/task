<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts._head')
  </head>
  <body>

    @include('layouts._nav')

    @include('layouts._alert')

    <div class="container">
      @yield('content')
    </div>

    @include('layouts._javascripts')

  </body>
</html>