let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'public/clients/css/bootstrap.min.css',
    'public/clients/css/parsley.css'
], 'public/css/all.css').version();


mix.scripts([
    'node_modules/jquery/dist/jquery.min.js',
    'public/clients/js/bootstrap.min.js',
    'public/clients/js/parsley.min.js',
    'node_modules/vue/dist/vue.js'
], 'public/js/all.js').version();

