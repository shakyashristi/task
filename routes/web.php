<?php

Route::group(['namespace' => 'Clients'], function(){
	Route::get('/', ['uses'=>'Home@index', 'as'=>'index']);

	Route::get('client/export', ['uses' => 'Client@export', 'as' => 'client.export']);
	Route::resource('client', 'Client', ['except'=>['edit', 'update', 'destroy']]);
});