** Task flow **

1. Study for the task
2. Development
3. Hosting

1. Study for the task
	- Detail study for the task was done since data need to be entered in .csv file.
	- Search for plugins.

2. Development
	- laravel 5.4
	- use plugin for csv data manpulation version 8.0. 
		- http://csv.thephpleague.com/8.0/
		- this is used for read and write operation in csv file
		- Client controller used.
		- ClientClass for interaction with csv file.
	- pagination done
		- app/Paginate.php for the paination methods and links.
	- use of plugin for captcha
		- https://github.com/mewebstudio/captcha
		- captcha used for form submission
	- use of plugin for excel export
		- http://www.maatwebsite.nl/laravel-excel/docs/getting-started
		- for excel export of data.
	- use of webpack mix for compiling assets
	-testing
		- unable to use any testing method.

3. Hosting 
	- uploaded in task.shristishakya.com.np
	- uploaded project using FTP (filezilla)
	- hosting in task.shristishakya.com.np did not support laravel 5.5 . So project shfed to laravel 5.4

